package ejercicio04;

public class Ejercicio04 {

	public static void main(String[] args) {
		// Enteros: byte, Short, int, long
		String nombre = "Elena";
		byte edad;
		short sueldo;

		// decimales: float , double
		float nota = 5.6f;
		double media = 5.6789;

		nombre = " Inma Estallo";
		edad = 99;
		sueldo = 30000;

		System.out.println("Hola" + nombre);
		System.out.println("La edad es " + edad);
		System.out.println("Tu sueldo es " + sueldo);

	}
}
